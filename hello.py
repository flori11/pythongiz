
print('hello world')

# variables

x = 5 # integer
y = 4.2 # float

z = 'hello'

a = True # or False

b = x + y

n = z + ' world' # CONCATENATION

o = 5 + 10

print(o)


"""
bash commands
ls # list files in current directory
cd folder # navigate
mkdir folder # make directory
cp file # copy file
rm file # delete file
rm -r folder # delete folder

"""